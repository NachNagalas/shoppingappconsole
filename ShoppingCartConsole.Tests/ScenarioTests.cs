using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;
using ShoppingCartConsole.Contracts.Models;
using ShoppingCartConsole.Contracts.Exceptions;

namespace ShoppingCartConsole.Tests
{
    public class ScenarioTests
    {
        [Fact]
        public void ShopContainsAtLeastTenTypesOfItems()
        {
            var newShop = new Shop();
            Assert.NotInRange(newShop.CountItemTypes(), 0, 9);
        }
        [Fact]
        public void CanEnterMultipleItems()
        {
            var itemTypeA = new ItemType("ItemTypeA",1);
            var itemTypeB = new ItemType("ItemTypeB",1);
            var newShoppingCart = new ShoppingCart();
            var cartItemAlpha = new CartItem(new Item(itemTypeA, 5), 1);
            var cartItemBeta = new CartItem(new Item(itemTypeB, 5), 1);

            newShoppingCart.AddMultipleItem(new List<CartItem> { cartItemAlpha, cartItemBeta });

            Assert.NotEmpty(newShoppingCart.CartItems);
        }
        [Fact]
        public void CanCalculateMultipleItemCost()
        {
            var itemTypeA = new ItemType("ItemTypeA",1);
            var itemTypeB = new ItemType("ItemTypeB",1);

            var newShoppingCart = new ShoppingCart();


            var cartItemAlpha = new CartItem(new Item(itemTypeA, 5), 1);
            var cartItemBeta = new CartItem(new Item(itemTypeB, 5), 1);

            newShoppingCart.AddMultipleItem(new List<CartItem> { cartItemAlpha, cartItemBeta });

            Assert.Equal(10, newShoppingCart.TotalCost());
        }
        [Fact]
        public void ItemCanBeDiscountedPercentage()
        {
            var itemTypeA = new ItemType("ItemTypeA",1);
            var itemAlpha = new Item(itemTypeA, 5);

            itemAlpha.ApplyDiscountPercentage(10);

            Assert.Equal(0.1, itemAlpha.DiscountPercentage);
        }
        [Fact]
        public void ItemCanBeDiscountedFlat()
        {
            var itemTypeA = new ItemType("ItemTypeA",1);
            var itemAlpha = new Item(itemTypeA, 5);

            itemAlpha.ApplyDiscountFlat(2);

            Assert.Equal(2, itemAlpha.DiscountFlat);
        }
        [Fact]
        public void ItemCanHaveBothDiscountType()
        {
            var itemTypeA = new ItemType("ItemTypeA",1);
            var itemAlpha = new Item(itemTypeA, 5);
            itemAlpha.ApplyDiscountFlat(2);
            itemAlpha.ApplyDiscountPercentage(10);

            Assert.NotEqual(3, itemAlpha.Discount);
        }

        [Fact]
        public void CanApplyCoupon()
        {
            var itemTypeA = new ItemType("ItemTypeA",1);
            var itemTypeB = new ItemType("ItemTypeB",1);

            var newCoupon = new Coupon(10);

            var newShoppingCart = new ShoppingCart();

            var cartItemAlpha = new CartItem(new Item(itemTypeA, 5), 1);
            var cartItemBeta = new CartItem(new Item(itemTypeB, 5), 1);

            newShoppingCart.AddMultipleItem(new List<CartItem> { cartItemAlpha, cartItemBeta });

            newShoppingCart.ApplyCoupon(newCoupon);

            Assert.Equal(1, newShoppingCart.CartItems.Sum(x => x.Item.Discount));
        }
        [Fact]
        public void ApplyCouponOnlyHighestDiscounts()
        {
            var itemTypeA = new ItemType("ItemTypeA",1);
            var newCoupon = new Coupon(10);
            var newShoppingCart = new ShoppingCart();

            var cartItemAlpha = new CartItem(new Item(itemTypeA, 5), 1);
            cartItemAlpha.Item.ApplyDiscountFlat(2);
            cartItemAlpha.Item.ApplyDiscountPercentage(20);

            newShoppingCart.AddItem(cartItemAlpha);

            newShoppingCart.ApplyCoupon(newCoupon);

            Assert.Equal(2, newShoppingCart.CartItems.Sum(x => x.Item.Discount));
        }
        [Fact]
        public void ApplyCouponByItemTypeOnlyHighestDiscounts()
        {
            var itemTypeA = new ItemType("ItemTypeA",1);
            var itemTypeB = new ItemType("ItemTypeB",1);

            var newCoupon = new Coupon("ItemTypeA", 10);

            var newShoppingCart = new ShoppingCart();

            var cartItemAlpha = new CartItem(new Item(itemTypeA, 5), 1);            
            cartItemAlpha.Item.ApplyDiscountFlat(2);
            cartItemAlpha.Item.ApplyDiscountPercentage(20);

            var cartItemBeta = new CartItem(new Item(itemTypeB, 5), 1);

            newShoppingCart.AddMultipleItem(new List<CartItem> { cartItemAlpha, cartItemBeta });

            newShoppingCart.ApplyCoupon(newCoupon);

            Assert.Equal(2, newShoppingCart.CartItems.Sum(x => x.Item.Discount));
        }

        [Fact]
        public void CanOnlyUseOneCoupon()
        {
            var newCoupon = new Coupon("ItemTypeA", 10);

            var newShoppingCart = new ShoppingCart();

            newShoppingCart.ApplyCoupon(newCoupon);

            var exception = Record.Exception(() => newShoppingCart.ApplyCoupon(newCoupon));
            Assert.IsType(typeof(DuplicateCouponException), exception);
        }

        [Fact]
        public void CalculateShippingCost()
        {
            var itemTypeA = new ItemType("ItemTypeA",1);

            var newShoppingCart = new ShoppingCart();

            var cartItemAlpha = new CartItem(new Item(itemTypeA, 5), 1); 

            newShoppingCart.AddItem(cartItemAlpha);

            var shippingCost = newShoppingCart.CalculateShippingCost();

            Assert.Equal(3.50, shippingCost);
        }

        [Fact]
        public void CalculateTotal()
        {
            var itemTypeA = new ItemType("ItemTypeA",1);

            var newShoppingCart = new ShoppingCart();

            var cartItemAlpha = new CartItem(new Item(itemTypeA, 5), 3);
            cartItemAlpha.Item.ApplyDiscountFlat(1); 

            newShoppingCart.AddItem(cartItemAlpha);

            var totalCost = newShoppingCart.CalculateTotal();

            Assert.Equal(15.50, totalCost);
        }
    }
}
