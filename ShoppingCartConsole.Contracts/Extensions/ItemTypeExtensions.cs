using System;
using ShoppingCartConsole.Contracts.Models;

namespace ShoppingCartConsole.Contracts.Extensions
{
    public static class ItemTypeExtensions
    {
        public static double ShippingCost(this ItemType itemType, int quantity)
        {
            var shippingCost = 0.00;

            switch (itemType.Weight * quantity)
            {
                case double itemTypeWeight when (itemTypeWeight > 8000):
                    var excessWeight = itemTypeWeight - 8000;
                    shippingCost = 50 + ((excessWeight / 1000) * 5);
                    break;
                case double itemTypeWeight when (itemTypeWeight >= 3000):
                    shippingCost = 50;
                    break;
                case double itemTypeWeight when (itemTypeWeight >= 1000):
                    shippingCost = 35;
                    break;
                case double itemTypeWeight when (itemTypeWeight >= 500):
                    shippingCost = 22;
                    break;
                case double itemTypeWeight when (itemTypeWeight >= 200):
                    shippingCost = 15;
                    break;
                case double itemTypeWeight when (itemTypeWeight >= 76):
                    shippingCost = 8;
                    break;
                case double itemTypeWeight when (itemTypeWeight >= 0):
                    shippingCost = 3.50;
                    break;
            }

            return shippingCost;
        }
    }
}