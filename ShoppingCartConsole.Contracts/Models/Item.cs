using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace ShoppingCartConsole.Contracts.Models
{
    public class Item
    {
        public Item(ItemType itemType, double cost)
        {
            Cost = cost;
            ItemType = itemType;
        }

        public double Cost { get; }
        public double DiscountPercentage { get; private set; }
        public double DiscountFlat { get; private set; }
        public ItemType ItemType { get; }
        public double Discount
        {
            get
            {
                var appliedDiscountPercentage = Cost * DiscountPercentage;

                if (appliedDiscountPercentage > DiscountFlat)
                {
                    return appliedDiscountPercentage;
                }
                else
                {
                    return DiscountFlat;
                }
            }
        }
        public void ApplyDiscountPercentage(int percentage)
        {
            DiscountPercentage = percentage * .01;
        }

        public void ApplyDiscountFlat(double amount)
        {
            DiscountFlat = amount;
        }
    }
}