using System;
using System.Collections.Generic;
using ShoppingCartConsole.Contracts.Exceptions;
using ShoppingCartConsole.Contracts.Extensions;

namespace ShoppingCartConsole.Contracts.Models
{
    public class ShoppingCart
    {
        public ShoppingCart()
        {
        }

        private List<CartItem> _cartItems { get; set; } = new List<CartItem>();
        public List<CartItem> CartItems { get { return _cartItems; } }
        public bool CouponApplied { get; private set; } = false;

        public double TotalCost()
        {
            var totalCost = 0.0;
            _cartItems.ForEach(x => totalCost += (x.Item.Cost - x.Item.Discount) * x.Quantity);
            return totalCost;
        }

        public void AddItem(CartItem itemCart)
        {
            _cartItems.Add(itemCart);
        }

        public void AddMultipleItem(IEnumerable<CartItem> items)
        {
            var addedItemList = new List<CartItem>(items);
            addedItemList.ForEach(x => AddItem(x));
        }

        public void ApplyCoupon(Coupon coupon)
        {
            if (CouponApplied)
            {
                Console.WriteLine("Unable to apply coupon. Coupon already applied.");
                throw new DuplicateCouponException();
            }

            _cartItems.ForEach(x =>
            {
                {
                    if (coupon.CouponType == CouponType.Type && x.Item.ItemType.Code != coupon.ItemTypeCode)
                    {
                        return;
                    }
                    else
                    {
                        if (x.Item.DiscountPercentage < coupon.DiscountPercentage)
                        {
                            x.Item.ApplyDiscountPercentage(coupon.DiscountPercentage);
                        }
                    }
                }
            });

            CouponApplied = true;
        }

        public double CalculateShippingCost()
        {
            var shippingCost = 0.0;
            _cartItems.ForEach(
                x =>
                {
                    shippingCost += x.Item.ItemType.ShippingCost(x.Quantity);
                }
            );
            return shippingCost;
        }

        public object CalculateTotal()
        {
            var total = 0.0;
            _cartItems.ForEach(
            x =>
            {
                total += ((x.Item.Cost - x.Item.Discount) * x.Quantity) + CalculateShippingCost();
            }
            );

            return total;
        }
    }
}