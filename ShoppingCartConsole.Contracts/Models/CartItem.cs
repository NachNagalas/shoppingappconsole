namespace ShoppingCartConsole.Contracts.Models
{
    public class CartItem
    {
        public CartItem(Item item, int quantity)
        {
            Item = item;
            Quantity = quantity;
        }

        public Item Item { get; }
        public int Quantity { get; }
    }
}