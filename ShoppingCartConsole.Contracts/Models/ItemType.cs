namespace ShoppingCartConsole.Contracts.Models
{
    public class ItemType
    {
        public ItemType(string code, double weight)
        {
            Code = code;
            Weight = weight;
        }

        public string Code { get; }
        public double Weight { get; }
    }
}