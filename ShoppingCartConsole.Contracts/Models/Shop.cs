using System;
using System.Collections.Generic;

namespace ShoppingCartConsole.Contracts.Models
{
    public class Shop
    {
        public Shop()
        {
            GenerateDefaultItemTypes();
        }

        private List<ItemType> _itemTypes { get; set; } = new List<ItemType>();
        public List<ItemType> ItemTypes { get { return _itemTypes; } }

        public int CountItemTypes()
        {
            return ItemTypes.Count;
        }

        private void GenerateDefaultItemTypes()
        {
            while(_itemTypes.Count < 10)
            {
                _itemTypes.Add(new ItemType($"ItemType{_itemTypes.Count}", 1));
            }
        }
    }
}