namespace ShoppingCartConsole.Contracts.Models
{
    public class Coupon
    {
        public Coupon(int percentage)
        {
            DiscountPercentage = percentage;
            CouponType = CouponType.All;
        }
        public Coupon(string itemTypeCode, int percentage)
        {
            DiscountPercentage = percentage;
            CouponType = CouponType.Type;
            ItemTypeCode = itemTypeCode;
        }

        public int DiscountPercentage { get; }
        public CouponType CouponType { get; }
        public string ItemTypeCode { get; }
    }
}